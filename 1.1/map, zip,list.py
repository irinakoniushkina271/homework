#1
print("1.",list(map(lambda i: i*2, range(-20, 4, 2))))
#2
l1 = [int(i) for i in range(1,5)]
l2 = [int(i) for i in range(5,10)]
l3 = [int(i) for i in range(10,15)]
print("2.", sum(map(lambda x, y, z: [x*y*z], l1, l2, l3), []))
#3
l = [134, 0, 'moon', 'picture', 44004]
print("3.", [(len(str(i))) for i in l])
#4
print('4.', [int(i) for i in range(2, 15,2)])
#5
l = ['A', 'B', '', 'C', '', 'D']
print('5.', list(filter(None, l)))
#6
l = [1, 2, 3, 4, 5, 6]
m = ['a', 'b', 'c', 'd', 'e', 'f']
k = [21, 32, 34, 56, 44, 765]
print('6.', list(zip(l, m, k)))
#7
f = [int(i) for i in range(15, 22)]
s = [int(i) for i in range(25, 32)]
spisok = sum(map(lambda x: [x*2], s), [])
print('7.', list(zip(f, spisok)))
