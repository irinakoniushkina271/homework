import pygame

pygame.init()

top_surf=pygame.display.set_mode((500,500))
game_clock=pygame.time.Clock()
top_surf.fill((255, 255, 255))

m1=pygame.sprite.Sprite()
m1.image=pygame.image.load('./animations/l1.png')
m1.rect=m1.image.get_rect()
m1.rect.move_ip(475, 0)

m2=pygame.sprite.Sprite()
m2.image=pygame.image.load('./animations/r1.png')
m2.rect=m2.image.get_rect()

move_right = ["./animations/r1.png", "./animations/r2.png", "./animations/r3.png", "./animations/r4.png", "./animations/r5.png"]
move_left = ["./animations/l1.png", "./animations/l2.png", "./animations/l3.png", "./animations/l4.png", "./animations/l5.png"]
runing=True
while runing:
    top_surf.fill((255, 255, 255))
    top_surf.blit(m1.image, m1.rect)
    top_surf.blit(m2.image, m2.rect)
    if pygame.sprite.collide_rect(m1, m2):
        runing = False
        pygame.quit()
        exit()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    if pygame.key.get_pressed()[pygame.K_d]:
        for i in move_right:
            m1.image=pygame.image.load(i)
            move_right.remove(i)
            move_right.append(i)
            if m1.rect.left > 480:
                break
            else:
                m1.rect.move_ip(1, 0)
    if pygame.key.get_pressed()[pygame.K_a]:
        for i in move_left:
            m1.image=pygame.image.load(i)
            move_left.remove(i)
            move_left.append(i)
            if m1.rect.left < 5:
                break
            else:
                m1.rect.move_ip(-1, 0)
    if pygame.key.get_pressed()[pygame.K_s]:
        for i in move_right:
            m1.image=pygame.image.load(i)
            move_right.remove(i)
            move_right.append(i)
            if m1.rect.bottom > 495:
                break
            else:
                m1.rect.move_ip(0, 1)
    if pygame.key.get_pressed()[pygame.K_w]:
        for i in move_left:
            m1.image=pygame.image.load(i)
            move_left.remove(i)
            move_left.append(i)
            if m1.rect.top < 5:
                break
            else:
                m1.rect.move_ip(0, -1)

    if pygame.key.get_pressed()[pygame.K_RIGHT]:
        for i in move_right:
            m2.image=pygame.image.load(i)
            move_right.remove(i)
            move_right.append(i)
            if m2.rect.right > 495:
                break
            else:
                m2.rect.move_ip(1, 0)
    if pygame.key.get_pressed()[pygame.K_LEFT]:
        for i in move_left:
            m2.image=pygame.image.load(i)
            move_left.remove(i)
            move_left.append(i)
            if m2.rect.left < 5:
                break
            else:
                m2.rect.move_ip(-1, 0)
    if pygame.key.get_pressed()[pygame.K_DOWN]:
        for i in move_right:
            m2.image=pygame.image.load(i)
            move_right.remove(i)
            move_right.append(i)
            if m2.rect.bottom > 495:
                break
            else:
                m2.rect.move_ip(0, 1)
    if pygame.key.get_pressed()[pygame.K_UP]:
        for i in move_left:
            m2.image=pygame.image.load(i)
            move_left.remove(i)
            move_left.append(i)
            if m2.rect.top < 5:
                break
            else:
                m2.rect.move_ip(0, -1)

    pygame.display.flip()
    game_clock.tick(25)
