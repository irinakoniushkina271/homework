import pygame
import sys
import random
from tkinter import *
from tkinter.messagebox import askyesno
from tkinter import ttk
pygame.mixer.init()
pygame.init()

COUNT_BLOCK = 20
SIZE_BLOCK = 20
GREEN = (204, 255, 153)
WHITE = (255, 255, 255)
FRAME_COLOR = (178, 255, 102)
SNAKE_COLOR = (255, 128, 0)
RED = (224, 0, 0)
HEADER_MARGIN = 70
apple_sound = pygame.mixer.Sound('499fe33297885e4.mp3')
over_sound = pygame.mixer.Sound('stolknovenie-razrusheniya-treniya-igryi-kollektsiya-ustranit-42222.mp3')
pygame.mixer.music.load('8109d2025e341b0.mp3')
pygame.mixer.music.play(-1)

size = [SIZE_BLOCK * COUNT_BLOCK + 2 * SIZE_BLOCK,
        SIZE_BLOCK * COUNT_BLOCK + 2 * SIZE_BLOCK + HEADER_MARGIN]
screen = pygame.display.set_mode(size)
pygame.display.set_caption('Змейка')
timer = pygame.time.Clock()
courier = pygame.font.SysFont('courier', 36)

class SnakeBlock:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def confict(self):
        return 0 <= self.x < COUNT_BLOCK and 0 <= self.y < COUNT_BLOCK

    def __eq__(self, other):
        return isinstance(other, SnakeBlock) and self.x == other.x and self.y == other.y

def window():
    result = askyesno(title="Game over", message=f'Ваш результат: {total}\nПродолжить игру?')
    if result:
        running = True
    else:
        pygame.quit()
        sys.exit()

def get_random_empty_block():
    x = random.randint(0, COUNT_BLOCK - 1)
    y = random.randint(0, COUNT_BLOCK - 1)
    empty_block = SnakeBlock(x, y)
    while empty_block in snake_blocks:
        empty_block.x = random.randint(0, COUNT_BLOCK - 1)
        empty_block.y = random.randint(0, COUNT_BLOCK - 1)
    return empty_block
def drow_block(color, row, column):
    pygame.draw.rect(screen, color, [SIZE_BLOCK + column * SIZE_BLOCK,
                                     HEADER_MARGIN + SIZE_BLOCK + row * SIZE_BLOCK, SIZE_BLOCK,
                                     SIZE_BLOCK])

snake_blocks = [SnakeBlock(9, 8), SnakeBlock(9, 9), SnakeBlock(9, 10)]
apple = get_random_empty_block()
d_row = buf_row = 0
d_col = buf_col = 1
total = 0
speed = 1

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP and d_col != 0:
                buf_row = -1
                buf_col = 0
            elif event.key == pygame.K_DOWN and d_col != 0:
                buf_row = 1
                buf_col = 0
            elif event.key == pygame.K_LEFT and d_row != 0:
                buf_row = 0
                buf_col = -1
            elif event.key == pygame.K_RIGHT and d_row != 0:
                buf_row = 0
                buf_col = 1


    screen.fill(FRAME_COLOR)
    pygame.draw.rect(screen, FRAME_COLOR, [0, 0, size[0], HEADER_MARGIN])
    text_total = courier.render(f"Total: {total}", 0, WHITE)
    screen.blit(text_total, (SIZE_BLOCK, SIZE_BLOCK))

    for row in range(COUNT_BLOCK):
        for column in range(COUNT_BLOCK):
            drow_block(GREEN, row, column)

    head = snake_blocks[-1]
    if not head.confict():
        over_sound.play()
        window()
        running = False

    drow_block(RED, apple.x, apple.y)
    for block in snake_blocks:
        drow_block(SNAKE_COLOR, block.x, block.y)

    if apple == head:
        total += 1
        speed = total // 5 + 1
        snake_blocks.append(apple)
        apple = get_random_empty_block()
        pygame.mixer.Sound.play(apple_sound)

    d_row = buf_row
    d_col = buf_col
    new_head = SnakeBlock(head.x + d_row, head.y + d_col)

    if new_head in snake_blocks:
        over_sound.play()
        pygame.quit()
        sys.exit()

    snake_blocks.append(new_head)
    snake_blocks.pop(0)
    pygame.display.flip()
    timer.tick(3 + speed)


