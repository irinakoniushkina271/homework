from tkinter import *
f = open("test.txt", encoding="utf-8").readlines()

window = Tk()
window.title('Тест по Tkinter')
window.geometry('800x500')
window.configure(bg='lightgreen')

frame = Frame(window)
frame.pack(expand=True)

score = 0

#списки вопросов и ответов
correct_answer = []
question = []
answer = []
for i in f:
    if i[0] == 'q':
        question.append(i[2:])
        print(i[2])
    else:
        if i.endswith('+\n'):
            answer.append(i[2:-2])
            correct_answer.append(i[2:-2])
        else:
            answer.append(i[2:-1])
answer_for_checking = answer.copy()
question_for_checking = question.copy()
#генерация вопросов
number = 0 #номер вопроса
def gen_question():
    global number, main_question, ans, frame1, button
    i = question[0]
    main_question = Label(frame, text=i, font="calibre 13 bold", height=3, relief=RIDGE)
    main_question.pack()
    question.remove(i)
    frame1 = Frame(window)
    frame1.pack(expand=True)
    num = -1
    for i in answer[:4]:
        num += 1
        ans = Radiobutton(frame1, width=30, text=i, font="calibre 14", variable=var, value=num, relief=RIDGE)
        ans.pack(padx=10, pady=10)
        answer.remove(i)
    button = Button(text="Дальше", font="calibre 14 bold", command=selected)
    button.pack(padx=10, pady=30)

#варианты ответов
var = IntVar()
var.set(0)

#получаем ответ пользователя
def selected():
    global score, number, result, proc
    number += 1
    for j in answer_for_checking[:4]:
        if var.get() == answer_for_checking.index(j) and j in correct_answer:
            score += 1

    if number <= 4:
        main_question.destroy()
        for i in range(4):
            frame1.destroy()
            button.destroy()
        gen_question()
    else:
        main_question.destroy()
        for i in range(4):
            frame1.destroy()
            frame.destroy()
            button.destroy()
        result = Label(window, text=f"Результат: {score}/5", font="calibre 15 bold", width=50, height= 10, relief=RIDGE)
        result.pack(expand=True)
    print(score, number)

gen_question()
window.mainloop()
