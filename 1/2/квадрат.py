import pygame

pygame.init()

screen = pygame.display.set_mode((300, 300))
rect = pygame.Rect(10, 10, 20, 20)
white = (255, 255, 255)
blue = (64, 128, 255)
green = (0, 200, 64)
yellow = (225, 225, 0)
pink = (230, 50, 230)
red = (250, 0, 0)
orange = (228, 151, 64)
colors = [red, orange, yellow, green, blue, pink, white]
col = (255, 255, 255)
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
            
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
                for i in colors:
                    col = i
                    colors.remove(i)
                    colors.append(i)
                    break 
            elif event.key == pygame.K_LEFT:
                if rect.left < 20:
                    break
                else:
                    rect.move_ip(-15, 0)
            elif event.key == pygame.K_RIGHT:
                if rect.right > 270:
                    break
                else:
                    rect.move_ip(15, 0)
            elif event.key == pygame.K_UP:
                if rect.top < 20:
                    break
                rect.move_ip(0, -15)
            elif event.key == pygame.K_DOWN:
                if rect.bottom > 280:
                    break
                else:
                    rect.move_ip(0, 15)

    pygame.draw.rect(screen, col, rect, 0)

    pygame.display.flip()
