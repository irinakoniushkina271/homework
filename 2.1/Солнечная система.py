import pygame
import math

pygame.init()
width = 1200
height = 820
YELLOW = (255, 221, 48)
COL_MERC = (220, 115, 44)
COL_VENERA = (239, 216, 123)
COL_EARTH = (108, 150, 223)
COL_MARS = (249, 137, 62)
COL_UPITER = (192, 180, 172)
COL_SATURN = (209, 164, 135)
COL_URAN = (192, 222, 229)
COL_NEPTUN = (50, 112, 219)

screen = pygame.display.set_mode((width, height))
class Planets():
    def __init__(self, name, color, radius, distance, dist_real, angle, move):
        self.radius = radius
        self.dist = distance
        self.angle = angle
        self.move = move
        self.name = name
        self.color = color
        self.dist_real = dist_real
    def draw(self):
        x = int(width / 2 + math.cos(math.radians(self.angle)) * self.dist)
        y = int(height / 2 + math.sin(math.radians(self.angle)) * self.dist)
        rad = self.radius / 1.2
        pygame.draw.circle(screen, self.color, (width/2, height/2), math.cos(math.radians(1)) * self.dist, 1)
        pygame.draw.circle(screen, self.color, (x, y), rad)
    def update(self):
        self.angle += self.move
    def score(self):
        self.move *= 10
    def slow(self):
        self.move /= 10

merc = Planets("Меркурий", COL_MERC, 2.44, 23, 0.387, 0, 1.24)
venera = Planets("Венера", COL_VENERA, 6.129, 33, 0.723, 0, 1.62)
earth = Planets("Земля", COL_EARTH, 6.378, 50, 1, 0, 1)
mars = Planets("Марс", COL_MARS, 3.387, 65, 1.5237, 0, 0.5)
upiter = Planets("Юпитер", COL_UPITER, 70.4, 135, 5.2, 0, 0.05)
saturn = Planets("Сатурн", COL_SATURN, 60, 250, 9.54, 0, 0.016)
uran = Planets("Уран", COL_URAN, 25.9, 330, 19.2, 0, 0.008)
neptun = Planets("Нептун", COL_NEPTUN, 24.75, 380, 30, 0, 0.004)
planets = [merc, venera, earth, mars, upiter, saturn, uran, neptun]

running = True
days_counter = 0
paused = 0

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                for planet in planets:
                    planet.score()
            if event.key == pygame.K_LEFT:
                for planet in planets:
                    planet.slow()
            if event.key == pygame.K_SPACE:
                paused = not paused
    if not paused:
        for planet in planets:
            planet.update()

    screen.fill((0, 0, 0))
    pygame.draw.circle(screen, YELLOW, (width/2, height/2), 20/1.2)
    for planet in planets:
        planet.draw()
        if paused == 0 and planet == earth:
            days_counter += 1
        font = pygame.font.Font(None, 30)
        text_surface = font.render(f"Земные дни: {days_counter}", True, COL_EARTH)
        screen.blit(text_surface, (15, 15))

        mouse_pos = pygame.mouse.get_pos()
        x = int(width / 2 + math.cos(math.radians(planet.angle)) * planet.dist)
        y = int(height / 2 + math.sin(math.radians(planet.angle)) * planet.dist)
        planet_rect = pygame.Rect(x - planet.radius, y - planet.radius, planet.radius*2, planet.radius*2)
        if planet_rect.collidepoint(mouse_pos):
            text = [f"{planet.name.capitalize()}", f"Радиус - {planet.radius} тыс.км", f"Расстояние до Солнца - {planet.dist_real} a.e", f"Угол - {planet.angle}"]
            font = pygame.font.Font(None, 30)
            heig = 30
            for i in text:
                text_surface = font.render(i, True, COL_EARTH)
                heig += 25
                screen.blit(text_surface, (15, heig))
    pygame.display.flip()
    pygame.display.update()


