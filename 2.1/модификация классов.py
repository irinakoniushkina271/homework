class MyList(list):
    def __sub__(self, other):
        spisok = MyList()
        for i in self:
            if i not in other:
                spisok.append(i)
        return spisok

    def __truediv__(self, other):
        spisok = MyList()
        for i in self:
            if i % other == 0:
                spisok.append(i)
        return spisok

    def __str__(self):
        return super().__str__() + " (MyList)"

l1 = MyList([1, 2, 3])
l2 = MyList([3, 4, 5])
l3 = l2 - l1
l4 = l1 - l2
print(l1, l4, l3, l2/5)
