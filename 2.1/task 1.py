import random
from tkinter import *
from tkinter import ttk
from tkinter.messagebox import showerror
def task1():
    print("Задача 1. Условие: \nCоставить программу обмена значениями трех переменных а, b, c так, чтобы: \na присвоить значение b, \nb присвоилось значение c, \nс присвоить значение а.")
    while True:
        try:
            print("Введите значения переменных:")
            a, b, c = int(input("a = ")), int(input("b = ")), int(input("c = "))
            print(f'Ответ: a = {b}, b = {c}, c = {a}')
            break
        except ValueError:
            print("Вы ввели не число!")
def task2_1():
    print("Задача 2.1. Условие: \nКонсольная программа. Пользователь вводит 2 числа. Проверьте, что это именно числа, если это не так, \nто выведите пользователю ошибку и попросите ввести число снова. Когда пользователь ввел числа, выведите сумму этих чисел.")
    while True:
        try:
            n = int(input('Введите первое число: '))
            m = int(input('Введите второе число: '))
            print("Ответ: ", n+m)
            break
        except ValueError:
            print("Вы ввели не число!")
def task2_2():
    print("Задача 2.2. Условие: \nКонсольная программа. Пользователь вводит 2 числа. Проверьте, что это именно числа, если это не так, то выведите \nпользователю ошибку и попросите ввести число снова. Когда пользователь ввел числа, выведите сумму этих чисел.\nДоработайте задачу так, чтобы пользователь мог вводить n разных чисел. Предоставьте возможность ввести n самому пользователю.")
    running = True
    while running:
        try:
            n = int(input("Введите количество слагаемых: "))
            l = [0] * n
            for i in range(len(l)):
                l[i] = int(input(f"Введите {i+1} число: "))
            print("Ответ: ", sum(l))
            break
        except ValueError:
            print("Вы ввели не число!")
def task3():
    print("Задача 3. Условие: Дано число x, которое принимает значение от 0 до 100. Вычислите чему будет равно x^5")
    running = True
    while running:
        try:
            n = int(input("Введите число от 0 до 100: "))
            if n < 0 or n > 100:
                print("Вы вышли за диапозон. Введите другое число")
            else:
                running = False
                t3 = n ** 5
                print("Ответ:", t3)
        except ValueError:
            print("Вы ввели не число!")
def task3_2():
    print("Задача 3.2. Условие: Дано число x, которое принимает значение от 0 до 100. Вычислите чему будет равно x^5. \nИзмените задачу так, чтобы для вычисления степени использовалось только умножение.")
    running = True
    while running:
        try:
            n = int(input("Введите число от 0 до 100: "))
            if n < 0 or n > 100:
                print("Вы вышли за диапозон. Введите другое число")
            else:
                running = False
                t = 1
                for i in range(5):
                    t *= n
                print("Ответ:", t)
        except ValueError:
            print("Вы ввели не число!")
def task4():
    print("Задача 4. Условие: Пользователь может вводить число от 0 до 250. Проверьте, принадлежит ли введенное число числам Фибоначчи.")
    running = True
    while running:
        try:
            n = int(input("Введите число от 0 до 250: "))
            if n < 0 or n > 250:
                print("Вы вышли за диапозон. Введите другое число")
            else:
                running = False
                #
                a = []
                fib1 = -1
                fib2 = 1
                for i in range(0, n + 1):
                    fib1, fib2 = fib2, fib1 + fib2
                    a.append(fib2)
                if n in a:
                    print("Ответ:", n, "есть в последовательности Фиббоначи")
                else:
                    print("Ответ:", n, "нет в последовательности Фиббоначи")
        except ValueError:
            print("Вы ввели не число!")
def task5():
    print("Задача 5. Условие: Реализуйте программу двумя способами на определение времени года в зависимости от введенного месяца года. \n1 способ.")
    while True:
        try:
            date = int(input('Введите число месяца: '))
            winter = [1, 2, 12]
            spring = [3, 4, 5]
            summer = [6, 7, 8]
            autumn = [9, 10, 11]
            if date > 12 or date < 1:
                print('Введите число месяца (от 1 до 12)')
            else:
                if date in winter:
                    task1 = "Зима"
                elif date in summer:
                    task1 = 'Лето'
                elif date in spring:
                    task1 = 'Весна'
                else:
                    task1 = "Осень"
                print(f"Ответ: {task1}")
            break
        except ValueError:
            print("Вы ввели не число!")
def task5_2():
    print("Задача 5. Условие: Реализуйте программу двумя способами на определение времени года в зависимости от введенного месяца года. \n2 способ.")
    while True:
        try:
            date = int(input('Введите число месяца: '))
            if date > 12 or date < 1:
                print('Введите число месяца (от 1 до 12)')
            season = {'Весна': [3, 4, 5],
                      'Зима': [1, 2, 12],
                      'Лето': [6, 7, 8],
                      'Осень': [9, 10, 11]}
            for key in season:
                if date in season[key]:
                    print(key)
        except ValueError:
            print("Вы ввели не число!")
def task6():
    print("Задача 6. Условие: Посчитайте сумму, количество четных и нечетных чисел от 1 до N. N вводит пользователь.")
    while True:
        try:
            n = int(input("Введите число: "))
            odd_number = 0
            even_number = 0
            even = 0  # четные
            odd = 0  # нечетные
            for i in range(1, n + 1):
                if i % 2 == 0:
                    even_number += i
                    even += 1
                else:
                    odd_number += i
                    odd += 1
            print("Ответ: Сумма четных чисел -", even_number, "Количество -", even, "\nСумма нечетных чисел -", odd_number,
                  "Количество -", odd)
            break
        except ValueError:
            print("Вы ввели не число!")
def task7():
    print("Задача 7. Условие: Для каждого из чисел от 1 до N, где N меньше 250 выведите количество делителей. N вводит пользователь. \nВыведите число и через пробел количество его делителей. Делителем может быть 1. ")
    running = True
    while running:
        try:
            n = int(input("Введите число(число должно быть меньше 250): "))
            l = []
            if n >= 250:
                print("Вы вышли за диапозон. Введите другое число")
            else:
                cnt = 0
                for i in range(1, n + 1):
                    if n % i == 0:
                        l.append(i)
                        cnt += 1
                print(f"Ответ: {n} {cnt}")
                running = False
        except ValueError:
            print("Вы ввели не число!")
def task8():
    print("Задача 8. Условие: Найти все различные пифагоровы тройки из интервала от N до М")
    while True:
        try:
            n = int(input("Введите число:"))
            m = int(input("Введите число:"))
            l = []
            for i in range(n, m+1):
                s = ""
                for j in range(i, m+1):
                    for k in range(j, m+1):
                        if i**2 + j**2 == k**2:
                            s = str(i) + ", " + str(j) + ", " + str(k)
                            l.append(s)
            print(f"Пифагоровы тройки из интервала от {n} до {m}:")
            for i in l:
                print(i)
            break
        except ValueError:
            print("Вы ввели не число!")
def task9():
    print("Задача 9. Условие: Найти все целые числа из интервала от N до M, которые делятся на каждую из своих цифр.")
    while True:
        try:
            number1 = int(input("Введите число: "))
            number2 = int(input("Введите число: "))
            l = []
            for i in range(number1, number2):
                n = i
                cnt = 0
                for j in range(len(str(i))):
                    if i % 10 == 0:
                        break
                    else:
                        if i % (n % 10) == 0:
                            cnt += 1
                            n //= 10
                        if cnt == len(str(i)):
                            l.append(i)
            print(*l, sep=", ")
            break
        except ValueError:
            print("Вы ввели не число!")
def task10():
    print("Задание 9. Условие: Натуральное число называется совершенным, если оно равно сумме всех своих делителей, включая единицу. Вывести первые N (N<5) совершенных чисел на экран.")
    while True:
        try:
            n = int(input("Введите число: "))
            if n > 5:
                print("Число должно быть меньше 5")
            else:
                i = 0
                l = []
                while len(l) < n:
                    i += 1
                    sum = 0
                    for j in range(1, i):
                        if i % j == 0:
                            sum += j
                    if sum == i:
                        l.append(i)
                print(*l, sep=", ")
                break
        except ValueError:
            print("Вы ввели не число")
def task11():
    print("Задача 11. Условие: Задайте одномерный массив в коде и выведите в консоль последний элемент данного массива тремя способами")
    while True:
        try:
            n = int(input("Введите длину массива: "))
            arr = [random.randint(0, 9) for _ in range(n)]
            print("Массив:", *arr, sep=" ")
            print(arr[-1])
            for i in range(len(arr)):
                if len(arr) == i + 1:
                    print(arr[i])
            print(arr.pop())
            break
        except ValueError:
            print("Вы ввели не число")
def task12():
    print("Задание 12. Условие: Задайте одномерный массив в коде и выведите в консоль массив в обратном порядке.")
    while True:
        try:
            n = int(input("Введите длину массива: "))
            arr = [random.randint(0, 9) for _ in range(n)]
            print("Массив:", *arr, sep=" ")
            arr.reverse()
            print("Массив в обратном порядке:", *arr, sep=" ")
            break
        except ValueError:
            print("Вы ввели не число")
def task13():
    print("Задание 13. Условие: Реализуйте нахождение суммы элементов массива через рекурсию. Массив можно задать в коде.")
    n = 7
    len_arr = n - 1
    arr = [random.randint(0, 9) for _ in range(n)]
    print("Массив:", *arr, sep=" ")
    def rec(arr, len_arr):
        if len_arr == 0:
            return arr[0]
        return arr[len_arr] + rec(arr, len_arr-1)
    print("Ответ:", rec(arr, len_arr))

def task14():
    print("Задание 14. Условие: Реализуйте оконное приложение-конвертер рублей в доллары. Создайте окно ввода для суммы в рублях.")
    def calculate():
        try:
            if spisok.get() == " рубль, Россия -> доллар, США":
                rub = int(get_entry.get())
                dol = rub / 100
                res_entry.delete(0, "end")
                res_entry.insert(0, dol)
            else:
                dol = int(get_entry.get())
                rub = dol * 100
                res_entry.delete(0, "end")
                res_entry.insert(0, rub)
        except ValueError:
            showerror(title="Ошибка", message="Введено некоректное значение!")

    root = Tk()
    root.geometry('400x200')
    root.title("Приложение-конвертер рублей в доллары")
    name = Label(text="Приложение-конвертер рублей в доллары", font=("Arial", 12))
    name.pack()
    currency = [" рубль, Россия -> доллар, США", "доллар, США -> рубль, Россия"]
    cur_var = StringVar(value=currency[0])
    spisok = ttk.Combobox(textvariable=cur_var, values=currency, width=29)
    spisok.pack(pady=5)
    get_entry = Entry()
    get_entry.pack(pady=5)
    res_entry = Entry()
    res_entry.pack()
    button = Button(text="Первести", font=("Arial", 10), command=calculate)
    button.pack(pady=5)
    root.mainloop()

def task15():
    print("Задание 15. Условие: Реализуйте вывод таблицы умножения в консоль размером n на m \nкоторые вводит пользователь, но при этом они не могут быть больше 20 и меньше 5.")
    running = True
    while running:
        try:
            n = int(input("Введите первое число:"))
            m = int(input("Введите первое число:"))
            if n > 20 or n < 5 or m > 20 or m < 5:
                print("Число должно быть <= 20 и >= 5")
                continue
            else:
                for i in range(1, n+1):
                    for j in range(1, m+1):
                        print(i*j, end="\t")
                    print()
                break
        except ValueError:
            print("Вы ввели не число!")

def task16():
    print()



while True:
    print()
    cmd = input("Введите номер задания: ")
    match cmd:
        case "1":
            task1()
        case "2.1":
            task2_1()
        case "2.2":
            task2_2()
        case "3":
            task3()
        case "3.2":
            task3_2()
        case "4":
            task4()
        case "5":
            task5()
        case "5.2":
            task5_2()
        case "6":
            task6()
        case "7":
            task7()
        case "8":
            task8()
        case "9":
            task9()
        case "10":
            task10()
        case "11":
            task11()
        case "12":
            task12()
        case "13":
            task13()
        case "14":
            task14()
        case "15":
            task15()
        case "16":
            task16()
